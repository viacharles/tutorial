import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './modules/layout/layout.component';

const routes: Routes = [
  { path: '', pathMatch:'full', redirectTo: 't' },
  {
    path: 't',
    loadChildren: () =>
      import('./modules/tutorial/tutorial.module').then(
        (m) => m.TutorialModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
