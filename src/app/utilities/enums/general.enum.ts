export enum DeviceType {
  Desktop = 1,
  Table,
  Mobile,
}

export enum ActionType {
  Create = 1,
  Read,
  Update,
  Delete,
}
