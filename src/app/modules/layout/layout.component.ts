import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

enum ERoute {
  Carousel = 1,
  Test,
  CsvToJson,
  OverlayControlCenter,
  ReactiveFormsExample,
  StudentList,
  VirtualCurrency,
}

interface IRoute {
  name: string;
  path: string;
}

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  constructor(private router: Router) {}

  public isMenu = false;

  ngOnInit(): void {
    this.initialActiveRouterList();

  }

  /**
   * 按鈕名稱 對 路徑
   */
  private routerAndNameMap = new Map<ERoute, IRoute>([
    // ['廣告', 't/ad'],
    [ERoute.Carousel, { name: '輪播', path: 't/carousel-example' }],
    [ERoute.CsvToJson, { name: '資料型態轉換', path: 't/csv-to-json'}],
    // ['MomentSDK', 't/date-example'],
    [ERoute.OverlayControlCenter, { name: 'Overlay控制中心', path: 't/overlay-control-center-example'}],
    [ERoute.ReactiveFormsExample, { name: 'ReactiveForm表單驗證＆樣式', path: 't/reactive-forms-example'}],
    // ['Rxjs練習', 't/rxjs-practice'],
    [ERoute.StudentList, { name:'學校管理後台', path:'t/student-list'}],
    [ERoute.Test, { name:'Test', path:'t/test'}],
    // ['拖曳卡片', 't/touchmove'],
    [ERoute.VirtualCurrency, { name: '虛擬幣網站', path: 't/virtual-currency'}],
  ]);

  public activeRouterList = new Map();
  public inactiveRouterList = new Map();

  /**
   * 換頁
   */
  public toPage(path: string) {
    this.router.navigateByUrl(path);
    this.routerAndNameMap.values()
  }

  /**
   * 刪除 該功能欄
   */
  public reduceFunc(key: string, value: string) {
    this.activeRouterList.delete(key);
    this.inactiveRouterList.set(key, value);
  }

  /**
   * 增加 該功能欄
   */
  public addFunc(key: string, value: string) {
    this.inactiveRouterList.delete(key);
    this.activeRouterList.set(key, value);
  }

  /**
   * 重置 所有功能欄
   */
  public initialActiveRouterList() {
    for (let [key, value] of this.routerAndNameMap) {
      this.activeRouterList.set(key, value);
    }
    this.inactiveRouterList.clear();
  }

  /**
   * 開關 menu
   */
  public toggleMenu() {
    this.isMenu = !this.isMenu;
    console.log( this.isMenu );
  }
}
