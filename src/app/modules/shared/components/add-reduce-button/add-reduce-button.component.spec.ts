import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReduceButtonComponent } from './add-reduce-button.component';

describe('AddReduceButtonComponent', () => {
  let component: AddReduceButtonComponent;
  let fixture: ComponentFixture<AddReduceButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddReduceButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReduceButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
