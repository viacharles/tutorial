import { Component, OnInit } from '@angular/core';
import { OverlayService } from '../../services/overlay.service';

@Component({
  selector: 'app-overlay-control-center',
  templateUrl: './overlay-control-center.component.html',
  styleUrls: ['./overlay-control-center.component.scss']
})
export class OverlayControlCenterComponent implements OnInit {

  constructor(public $overlay: OverlayService) { }

  ngOnInit(): void {
  }

}
