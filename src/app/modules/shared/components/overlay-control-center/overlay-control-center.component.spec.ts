import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayControlCenterComponent } from './overlay-control-center.component';

describe('OverlayControlCenterComponent', () => {
  let component: OverlayControlCenterComponent;
  let fixture: ComponentFixture<OverlayControlCenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OverlayControlCenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayControlCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
