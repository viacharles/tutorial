import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HideWordsDirective } from './hide-words.directive';
import { TouchMoveDirective } from './touch-move.directive';
import { AdScrollControllerDirective } from './ad-scroll-controller.directive';
import { ActiveDialogDirective } from './active-dialog.directive';



@NgModule({
  declarations: [HideWordsDirective, TouchMoveDirective, AdScrollControllerDirective, ActiveDialogDirective, ],
  imports: [
    CommonModule
  ],
  exports: [
    HideWordsDirective,
    TouchMoveDirective,
    AdScrollControllerDirective,
    ActiveDialogDirective,
  ]
})
export class DirectiveModule { }
