import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { IDialog } from 'src/app/utilities/interfaces/general.interface';
import { OverlayService } from '../services/overlay.service';

@Directive({
  selector: '[appActiveDialog]'
})
export class ActiveDialogDirective {

@Input() dialog!: IDialog<any>;

  constructor(
    private element: ElementRef,
    private $overlay:OverlayService,
  ) { }

  @HostListener('click', ['$event.target']) onClick(target: HTMLElement) {
    if (!this.element.nativeElement.children[0].contains(target)) {
      if (this.dialog.config?.backdropClose) {
        this.$overlay.closeDialog(this.dialog);
      }
    }
  }

}
