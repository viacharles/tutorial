import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Directive({
  selector: '[appAdScrollController]',
})
export class AdScrollControllerDirective implements OnInit, OnDestroy {
  @Input() imgWidth = 330;
  @Input() time = 3;

  constructor(private element: ElementRef) {}

  @HostListener('scroll', ['$event']) onScroll(event: any) {
    this.current = Math.floor(event.target.scrollLeft / this.imgWidth);
  }

  @HostListener('touchmove', ['$event']) onTouch() {
    this.isTouched = true;
  }

  @HostListener('touchend', ['$event']) onTouchEnd() {
    this.current = Math.floor(
      this.element.nativeElement.scrollLeft / this.imgWidth
    );
    if (this.element.nativeElement.scrollLeft % this.imgWidth > 0) {
      this.switchToNext();
    }
    this.isTouched = false;
  }

  private current = 0;
  private total = 0;
  private isTouched = false;
  private timer$ = interval(this.time * 1000).pipe(
    filter(() => !this.isTouched)
  );

  private subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(this.timer$.subscribe(() => this.switchToNext()));
    this.total = this.element.nativeElement.children.length;
  }

  private switchToNext() {
    this.current = this.current < this.total - 1 ? this.current + 1 : 0;
    console.log(this.current);
    this.element.nativeElement.scrollLeft =
      this.current === 0 ? 0 : this.current * this.imgWidth;
    console.log(this.element.nativeElement.scrollLeft);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
