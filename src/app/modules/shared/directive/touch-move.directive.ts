import {
  Directive,
  ElementRef,
  HostListener,
  Renderer2,
} from '@angular/core';

@Directive({
  selector: '[appTouchMove]',
})
export class TouchMoveDirective {
  constructor(private element: ElementRef, private renderer: Renderer2) {}

  private isAnimation = false;
  private startPoint = 0;

  @HostListener('touchstart', ['$event']) onTouchStart(event: TouchEvent) {
    if (!this.isAnimation) {
      this.startPoint = event.touches[0].clientX;
    }
  }

  @HostListener('touchmove', ['$event']) onTouchMove(event: TouchEvent) {
    this.renderer.setStyle(
      this.element.nativeElement,
      'left',
      `${event.touches[0].clientX}px`
    );
  }

  @HostListener('touchend', ['$event']) onTouchEnd(event: TouchEvent) {
    if (Math.abs(event.changedTouches[0].clientX - this.startPoint) > 150) {
      this.fadeout(event.changedTouches[0].clientX - this.startPoint > 0);
    } else {
      this.backStartPoint();
    }
  }

  private fadeout(isRight: boolean) {
    this.isAnimation = true;
    this.renderer.addClass(
      this.element.nativeElement,
      isRight ? 'fadeout-right' : 'fadeout-left'
    );
    setTimeout(() => {
      this.isAnimation = false;
    }, 1000);
  }

  private backStartPoint() {
    this.isAnimation = true;
    this.renderer.setStyle(
      this.element.nativeElement,
      'left',
      `${this.startPoint}px`
    );
    this.renderer.setStyle(this.element.nativeElement, 'transition', '0.7s');
    setTimeout(() => {
      this.renderer.removeStyle(this.element.nativeElement, 'transition');
      this.isAnimation = false;
    }, 1000);
  }
}
