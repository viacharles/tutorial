import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectiveModule } from './directive/directive.module';
import { AddReduceButtonComponent } from './components/add-reduce-button/add-reduce-button.component';
import { OverlayModule } from '../tutorial/pages/overlay-control-center-example/overlays/overlay.module';



@NgModule({
  declarations: [
    AddReduceButtonComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DirectiveModule,
    ReactiveFormsModule,
    OverlayModule
  ],
  exports: [
    DirectiveModule,
    OverlayModule
  ]
})
export class ShareModule { }
