import { Injectable, Injector } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ActionType } from 'src/app/utilities/enums/general.enum';
import { IDialog, IDialogConfig } from 'src/app/utilities/interfaces/general.interface';
import { DialogComponent } from '../../tutorial/pages/overlay-control-center-example/overlays/dialog/dialog.component';

// dialog動作包含 ＣＲＵＤ動作 和 執行動作的dialog本身
interface IDialogAction {
  action: ActionType,
  dialog: IDialog<any>,
}

@Injectable({
  providedIn: 'root',
})

export class OverlayService {
  constructor(
    private injector: Injector
  ) {
    this.dialogAction$.subscribe(action => this.resolveDialogAction(action))
  }

  // 1. 建立dialog set容器(每個內容都是唯一值)
private dialogQue: Set<IDialog<any>> = new Set<IDialog<any>>();
private dialogInjectorMap = new Map<string, Injector>(null);
private dialogAction = new BehaviorSubject<IDialogAction|null>(null);
private dialogAction$ = this.dialogAction.asObservable().pipe(
  filter(action => action?.dialog?.component)
)

get dialogs() { return this.dialogQue; }

public getDialogInjector(dialog: IDialog<any>) {
return this.dialogInjectorMap.get(dialog.id)
}

public  toggleDialog<T>(dialog: any, config?: IDialogConfig<T>) {
  this.dialogAction.next({
    action: ActionType.Create,
    dialog: {
      id: new Date().toISOString(),
      component: dialog,
      config
    }
  })
}

public closeDialog(dialog: IDialog<any>) {
  this.dialogAction.next({
    action: ActionType.Delete,
    dialog
  });
}

public resolveDialogAction(resource: IDialogAction|null) {
switch (resource?.action) {
  case ActionType.Create: this.createDialog(resource.dialog); break;
  case ActionType.Delete: this.deleteDialog(resource.dialog); break;
}
  }


private createDialog(dialog: IDialog<any>) {
  if (!this.dialogQue.has(dialog)) {
    this.dialogQue.add(dialog);
    this.dialogInjectorMap.set(dialog.id, Injector.create({
      providers: [{
        provide: DialogComponent,
        useValue: dialog,
      }],
      parent: this.injector
    }));
} else {
  console.error(` dialog ${dialog.id} already exist ! `)
}
}

private deleteDialog(dialog: IDialog<any>) {
  if (this.dialogQue.has(dialog)) {
    this.dialogQue.delete(dialog);
    this.dialogInjectorMap.delete(dialog.id);
  } else {
    console.error(`dialog ${dialog.id} not found !`)
  }
}

  // private dialog: BehaviorSubject<IDialogConfig> = new BehaviorSubject(null);
  // public dialog$ = this.dialog.asObservable().pipe(
  //   filter(config => !!config),
  //   tap((dialog) => {
  //     const Config = {
  //       component: dialog,
  //       id: new Date().toISOString(),
  //     }
  //      this.dialogQue.add(Config);
  //     return Config
  //   })
  // );

  // public toggleDialog(dialog: any) {
  //   this.dialog.next(dialog);
  // }
}
