import { EAdminAuth } from "src/app/utilities/enums/admin.enums"

export class Student {
    constructor(
        public name: string,
        public score: number,
    ) {
        if (this.score > 96) {
            this.scoreClass = 'A+';
        }
        else if (this.score > 89 &&  this.score <= 96) {
            this.scoreClass = 'A';
        }
        else if (this.score > 79 && this.score <= 89) {
            this.scoreClass = 'B';
        }
        else if (this.score > 69 && this.score <= 79) {
            this.scoreClass = 'C';
        }
        else if (this.score > 60 && this.score <= 69) {
            this.scoreClass = 'D';
        }
        else {
            this.scoreClass = 'D-';
        }
    }
    public scoreClass: string = '-'
}

export class Admin {
    constructor(
        public name:string,
        public auth:EAdminAuth,
    ) { }
   public addStudent (list: Map< number, Student >, id: number, student: Student) {
        list.set(id, new Student(student.name, student.score) );
      }
}

export class Class {
    constructor(
        public id: string ,
        public students:Map<number, Student>,
        public admins:Admin[],
    ) { }

    public checkStudentExist(id: number) {
      this.students.has(id);
      this.students.get(id);
      this.students.delete(id);
      this.students.set(id, new Student('test', 650))
    }

    private initialStudents() {
      this.json.classA.forEach(student => this.students.set(student.id, new Student(student.name, student.score)));
    }

    private json = {
      classA: [
        {
          id: 0,
          name: 'test',
          score: 50
        }
      ]
    }
}

export const studentsA = new Map<number, Student> ([
  [0, new Student('萬汰', 100)],
  [1, new Student('蛤吉', 80)],
  [2, new Student('魯迪烏斯', 90)],
  [3, new Student('愛麗絲', 53)],
  [4, new Student('希露', 74)],
])

export const studentsB = new Map ([
    [0, new Student('千尋', 100)],
    [1, new Student('宗介', 80)],
    [2, new Student('巴魯', 90)],
    [3, new Student('阿席達卡', 53)],
    [4, new Student('錢婆婆', 74)],
  ])
