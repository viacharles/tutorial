import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TutorialRoutingModule } from './tutorial-routing.module';
import { AdComponent } from './pages/ad/ad.component';
import { CarouselExampleComponent } from './pages/carousel-example/carousel-example.component';
import { DateExampleComponent } from './pages/date-example/date-example.component';
import { DirectiveExampleComponent } from './pages/directive-example/directive-example.component';
import { ReactiveFormsExampleComponent } from './pages/reactive-forms-example/reactive-forms-example.component';
import { OverlayModule } from './pages/overlay-control-center-example/overlays/overlay.module';
import { RxjsPracticeComponent } from './pages/rxjs-practice/rxjs-practice.component';
import { TestComponent } from './pages/test/test.component';
import { TouchmoveComponent } from './pages/touchmove/touchmove.component';
import { VirtualCurrencyComponent } from './pages/virtual-currency/virtual-currency.component';
import { DirectiveModule } from '../shared/directive/directive.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverlayControlCenterExampleComponent } from './pages/overlay-control-center-example/overlay-control-center-example.component';


@NgModule({
  declarations: [
    AdComponent,
    CarouselExampleComponent,
    DateExampleComponent,
    DirectiveExampleComponent,
    OverlayControlCenterExampleComponent,
    ReactiveFormsExampleComponent,
    RxjsPracticeComponent,
    TestComponent,
    TouchmoveComponent,
    VirtualCurrencyComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TutorialRoutingModule,
    OverlayModule,
    DirectiveModule,
  ]
})
export class TutorialModule { }
