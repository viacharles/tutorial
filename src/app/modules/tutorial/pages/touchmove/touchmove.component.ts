import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
  ViewChildren,
} from '@angular/core';


interface IStartPoint {
  x: number;
  touchX: number;
}
@Component({
  selector: 'app-touchmove',
  templateUrl: './touchmove.component.html',
  styleUrls: ['./touchmove.component.scss'],
})
export class TouchmoveComponent implements OnInit {
  @ViewChildren('items') items: ElementRef|null = null;
  constructor(private render: Renderer2, elem: ElementRef) {}

  public currentTimer: any;
  public currentInterval: any;
  public isMove = false;
  public images = [
    'assets/img/1.png',
    'assets/img/2.png',
    'assets/img/3.png',
    'assets/img/3.png',
  ];
  public current = 0;
get previous() {
  return this.current - 1 < 0 ? this.images.length-1 : this.current - 1;
}
  get next() {
    return this.current + 1 < this.images.length ? this.current + 1 : 0;
  }
  get next2() {
    return this.next + 1 < this.images.length ? this.next + 1 : 0;
  }

  get film() {
    return [
      this.images[this.previous],
      this.images[this.current],
      this.images[this.next],
      this.images[this.next2],
    ];
  }

  ngOnInit(): void {
    this.currentInterval = setInterval(() => this.carousel(), 4000);
  }

  public onTouch(event: TouchEvent) {
    console.log(event);
    if (!this.isMove) {
      clearInterval(this.currentInterval);
      clearTimeout(this.currentTimer);
      this.render.setStyle(event.target, 'left', event.targetTouches[0].clientX)
    }
  }
  public touchEnd(event: TouchEvent) {
    if (this.currentInterval) {
      this.currentInterval = setInterval(() => this.carousel(), 4000);
    }
  }

  private carousel() {
    this.isMove = true;
    this.currentTimer = setTimeout(() => {
      this.current =
        this.current + 1 < this.images.length ? this.current + 1 : 0;
      this.isMove = false;
    }, 1000);
  }
}
