import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentListLayoutComponent } from './student-list-layout/student-list-layout.component';

const routes: Routes = [
  { path: '', pathMatch:"full" ,redirectTo:'index'},
  { path: 'index', component: StudentListLayoutComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentListRoutingModule { }
