import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentListRoutingModule } from './student-list-routing.module';
import { NavComponent } from './nav/nav.component';
import { StudentPageComponent } from './student-page/student-page.component';
import { DefaultPageComponent } from './default-page/default-page.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { StudentListLayoutComponent } from './student-list-layout/student-list-layout.component';
import { FormsModule } from '@angular/forms';
import { DirectiveModule } from 'src/app/modules/shared/directive/directive.module';
import { CardComponent } from './card/card/card.component';


@NgModule({
  declarations: [
    StudentListLayoutComponent,
    AdminPageComponent,
    DefaultPageComponent,
    StudentPageComponent,
    NavComponent,
    CardComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    DirectiveModule,
  ],
  exports:[
  ]
})
export class StudentListModule { }
