import { EPage } from 'src/app/utilities/enums/page.enum';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PopupBox } from 'src/app/utilities/abstract/popup-box.abstract';
import { Admin, Class, Student } from 'src/app/modules/shared/models/student.model';
import { StudentService } from 'src/app/modules/shared/services/student.service';



@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent extends PopupBox implements OnInit {

  @Output() emit = new EventEmitter<Student>();
  @Output() emitAdmin = new EventEmitter<Admin>();

  constructor(
    public $student: StudentService,
  ) {
    super();
  }

  get className() {
    const ID = this.$student.currentClass?.id;
    return ID ? `${ID}班` : "";
  }


  ngOnInit(): void {
  }
  public ClassList: boolean = false;
  public ClassTitle: boolean = false;
  public searchValue: string = '';
  public search() {
  }

  public setStudentPage(student: Student) {
    this.emit.emit(student);
    this.$student.page = EPage.StudentPage;
  }
  public setCurrentList(classroom: Class) {
    this.$student.currentClass = classroom;
    this.ClassTitle = true;
    this.ClassList = false;
  }

  public isAdminPage(admin: Admin) {
    this.emitAdmin.emit(admin);
    this.$student.page = EPage.AdminPage;
  }

  public isStudentList() {
    return this.$student.currentClass?.students.size;
  }
  // public search() {
  //   this.$student.page = EPage.
  // }
}
