import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  @Output() dataE: EventEmitter<string> = new EventEmitter;
  constructor() {}

  public CSVdata = '';
  public JSONdata: string[] = [];

  ngOnInit(): void {}

  get titles(): string[] {
    return this.CSVdata.split('\n')[0].split(',');
  }
  get fields(): string[] {
    return this.CSVdata.split('\n')
    .filter( (phase, index) => index != 0 && phase.indexOf(',') >= 0 && phase.replace(',', '') );
  }
  public convert(fields: string[]) {
    this.JSONdata = fields.map((field: string) => {
      const JSON: any = {};
      this.titles.forEach( (title, index) =>  JSON[title] = field.split(',')[index] );
      return JSON;
    });
  }
  public dataEmit() {
this.dataE.emit();
  }
}
