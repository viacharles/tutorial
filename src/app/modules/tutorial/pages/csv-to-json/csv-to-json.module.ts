import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CsvToJsonRoutingModule } from './csv-to-json-routing.module';
import { SideBarComponent } from './components/side-bar/side-bar.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CsvToJsonComponent } from './csv-to-json.component';
import { ExportComponent } from './pages/export/export.component';
import { EditComponent } from './pages/edit/edit.component';


@NgModule({
  declarations: [SideBarComponent, ExportComponent, EditComponent, CsvToJsonComponent],
  imports: [
    CommonModule,
    CsvToJsonRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class CsvToJsonModule { }
