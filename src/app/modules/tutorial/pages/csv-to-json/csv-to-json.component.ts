import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-csv-to-json',
  templateUrl: './csv-to-json.component.html',
  styleUrls: ['./csv-to-json.component.scss']
})
export class CsvToJsonComponent implements OnInit {

  constructor() { }
  public data: string[] = []
  ngOnInit(): void {
  }

}
