import { Component, ElementRef, OnChanges, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-directive-example',
  templateUrl: './directive-example.component.html',
  styleUrls: ['./directive-example.component.scss']
})
export class DirectiveExampleComponent implements OnInit {

@ViewChild('tbutton') button?: ElementRef;

  constructor() {
  }
  public words:string = '';
  public inputWords:string = '';

  ngOnInit(): void {
  }



  }
