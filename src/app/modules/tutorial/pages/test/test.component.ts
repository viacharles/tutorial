import { Component, OnInit } from '@angular/core';
import { from, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

export class Temperature {

  constructor(){}
  public value = 20;

  public randomChangeTemperature() {
    setTimeout(()=> {

    })
  }
  }

@Component({
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
  constructor() {}

  private students = [
    { name: 'Will', score: 100 },
    { name: 'Mike', score: 49 },
    { name: 'Leo', score: 36 },
    { name: 'XXX', score: 25 },
  ];

  private data = [
    {
      id: 1,
      name: 'Data 1',
      children: [
        {
          id: 11,
          name: 'Data 1-1',
        },
        {
          id: 12,
          name: 'Data 1-2',
        },
      ],
    },
    {
      id: 2,
      name: 'Data 2',
    },
  ];

  private traversTree(data) {
    data.forEach((item) => {
      console.log(item.name);
      if (!!item.children ) {
        this.traversTree(item.children)
      }
    });
  }

  private studentsSource$ = from(this.students).pipe(
    map((student) => {
      return {
        ...student,
        score: Math.sqrt(student.score),
      };
    }),
    map((student) => ({ ...student, score: student.score * 10 })),
    filter((student) => student.score >= 60)
  );

  private source$ = new Observable((subscriber) => {
    console.log('stream 開始');
    subscriber.next(1);
    subscriber.next(2);
    subscriber.next(3);
    setTimeout(() => {
      subscriber.next(4);
      subscriber.complete();
      console.log('stream 結束');
    }, 100);
  });



  ngOnInit(): void {
    this.source$.subscribe((value) => console.log(value));

    this.studentsSource$.subscribe((student) => console.log(student));

    this.traversTree(this.data);


    console.log(
      this.addTwoNumbers1(
        new ListNode(2, new ListNode(4, null)),
        new ListNode(5, new ListNode(6, new ListNode(4)))
      )
      // this.addTwoNumbers(
      //   new ListNode(
      //     9,
      //     new ListNode(
      //       9,
      //       new ListNode(
      //         9,
      //         new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9))))
      //       )
      //     )
      //   ),
      //   new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9))))
      // )
    );
  }

  // private addTwoNumbers(l1: number[], l2: number[]) {
  //   const LSmall = l1.length > l2.length ? l2 : l1;
  //   const LBig = l1.length > l2.length ? l1 : l2;
  //   const l3: number[] = [];
  //   let digits = false;
  //   LBig.forEach((number1, index) => {
  //     let number3 = digits ? number1 + (LSmall[index] || 0) + 1 : number1 + (LSmall[index] || 0);
  //     if (number3 >= 10) {
  //       digits = true;
  //       number3 = number3 - 10;
  //     } else {
  //       digits = false;
  //     }
  //     l3.push(number3);
  //   });
  //   if(digits) {
  //     l3.push(1);
  //   }
  //   return l3;
  // }

  private addTwoNumbers1(l1: ListNode, l2: ListNode) {
    let sum = l1.val + l2.val;
    if (l1.next || l2.next) {
      l1.next.val = l1.next.val + (sum >= 10 ? 1 : 0);
      return new ListNode(
        sum % 10,
        this.addTwoNumbers1(
          l1.next || new ListNode(0, null),
          l2.next || new ListNode(0, null)
        )
      );
    } else {
      return sum >= 10
        ? new ListNode(sum % 10, new ListNode(1, null))
        : new ListNode(sum);
    }
  }

  private addTwoNumbers2(l1: ListNode, l2: ListNode) {
    let sum = l1.val + l2.val;
    if (l1.next || l2.next) {
     if(l1.next) {
      l1.next.val = l1.next.val + (sum >= 10 ? 1 : 0);
     } else {
      l2.next.val = l2.next.val + (sum >= 10 ? 1 : 0);
     }
      return new ListNode(
        sum % 10,
        this.addTwoNumbers2(
          l1.next || new ListNode(0, null),
          l2.next || new ListNode(0, null)
        )
      );
    } else {
      return sum >= 10
        ? new ListNode(sum % 10, new ListNode(1, null))
        : new ListNode(sum);
    }
  }
}
