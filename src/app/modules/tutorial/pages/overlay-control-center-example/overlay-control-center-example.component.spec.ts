import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OverlayControlCenterExampleComponent } from './overlay-control-center-example.component';


describe('OverlayControlCenterComponent', () => {
  let component: OverlayControlCenterExampleComponent;
  let fixture: ComponentFixture<OverlayControlCenterExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OverlayControlCenterExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayControlCenterExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
