import { Component, OnInit } from '@angular/core';
import { OverlayService } from 'src/app/modules/shared/services/overlay.service';

@Component({
  selector: 'app-overlay-control-center-example',
  templateUrl: './overlay-control-center-example.component.html',
  styleUrls: ['./overlay-control-center-example.component.scss']
})
export class OverlayControlCenterExampleComponent implements OnInit {

  constructor(public $overlay: OverlayService,) { }

  ngOnInit(): void {
  }

}
