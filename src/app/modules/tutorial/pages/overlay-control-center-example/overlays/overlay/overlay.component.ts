import { Component } from '@angular/core';
import { OverlayService } from 'src/app/modules/shared/services/overlay.service';
import { IDialog } from 'src/app/utilities/interfaces/general.interface';

export interface IOverlay {
  title: string;
  callbacks?: BlobCallback;
}

@Component({
  selector: 'app-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss']
})

export class OverlayComponent {

  constructor(
    public $overlay: OverlayService,
  ) { }

// public toggleDialog() {
//   this.$overlay.toggleDialog<IOverlay>(OverlayComponent, {
//     backdropClose: true,
//     params: {
//       title: '修改密碼',
//       callbacks: {
//         close: this.onClose.bind(this),
//         submit: this.onSubmit.bind(this)
//       }
//     }
//   })
// }

private onClose(dialog: IDialog<any>) {

}

private onSubmit(data: any) {

}

}
