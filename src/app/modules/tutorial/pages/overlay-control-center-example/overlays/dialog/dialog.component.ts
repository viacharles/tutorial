import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IDialog, IDialogConfig } from 'src/app/utilities/interfaces/general.interface';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent<T> implements IDialog<T> {

  @Output() OnClose = new EventEmitter<void>();
  @Input() disableClose: any;

  constructor() { }

public id: string = '';
public component: any;
public config: IDialogConfig<T>|undefined;

}
