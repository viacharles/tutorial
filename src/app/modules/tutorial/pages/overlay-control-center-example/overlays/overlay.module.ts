import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from './dialog/dialog.component';
import { OverlayComponent } from './overlay/overlay.component';
import { WelcomDialogComponent } from './welcom-dialog/welcom-dialog.component';

@NgModule({
  declarations: [DialogComponent, OverlayComponent, WelcomDialogComponent],
  imports: [CommonModule],
  exports: [DialogComponent, OverlayComponent, WelcomDialogComponent],
})
export class OverlayModule {}
