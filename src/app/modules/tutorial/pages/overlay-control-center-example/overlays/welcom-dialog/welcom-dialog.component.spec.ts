import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomDialogComponent } from './welcom-dialog.component';

describe('WelcomDialogComponent', () => {
  let component: WelcomDialogComponent;
  let fixture: ComponentFixture<WelcomDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WelcomDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
