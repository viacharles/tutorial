import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

enum CollectionDateType {
  All = 1,
  Cashed,
}

@Component({
  selector: 'app-reactive-forms-example',
  templateUrl: './reactive-forms-example.component.html',
  styleUrls: ['./reactive-forms-example.component.scss'],
})
export class ReactiveFormsExampleComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    // public $overlay: OverlayService
    ) {}

  get collectionDate(): typeof CollectionDateType {
    return CollectionDateType;
  }

  public form3: FormGroup = this.fb.group({
    accounts: [null],
    method: [null],
    startNum: [null, [Validators.minLength(4), Validators.required]],
    endNum: [
      null,
      [Validators.required, Validators.minLength(4), Validators.maxLength(30)],
    ],
    amountRange: new FormControl(null,
  ),
  minAmount: [null],
  maxAmount: [null],
})

  expressType: any;
  public isShowWarn = false;
  public form: FormGroup = this.fb.group({
    email: ['', [Validators.required, this.emailValidator]],
    password: [
      '',
      [Validators.required, Validators.minLength(5), this.passwordValidator],
    ],
  });

  private onFormChanged$ = forkJoin([
    this.form3.controls.method.valueChanges.pipe(
      tap((value) => this.onMethodChanged(value))
    ),
  ]);

  ngOnInit(): void {
    this.onFormChanged$.subscribe(() => console.log('form changed'));
    this.initialForm();
  }

  public submit() {
    // this.$overlay.toggleDialog(WelcomDialogComponent)
  }

  public getErrorMsg(field: string): string {
    const Field = this.form.get(field);
    return Field?.hasError('required')
      ? '這是必填欄位'
      : Field?.getError('invalid').message;
  }

  private onMethodChanged(value: CollectionDateType) {
    console.log(value)
    switch (value) {
      case CollectionDateType.Cashed:
        this.form3.controls.startNum.enable();
        this.form3.controls.endNum.enable();
        break;
      default:
        this.form3.controls.startNum.disable();
        this.form3.controls.endNum.disable();
        break;
    }
  }

  private initialForm() {
    this.form3.controls.method.setValue(CollectionDateType.All);
  }

  private emailValidator(field: FormControl): ValidationErrors {
    console.log(field);
    return field.value.includes('mail')
      ? {}
      : {
          invalid: {
            message: '信箱格式錯誤',
          },
        };
  }
  private passwordValidator(field: FormControl): ValidationErrors {
    return field.value.length > 8
      ? {}
      : {
          invalid: {
            message: '須超過8個字',
          },
        };
  }

  //   public submit() {
  //     console.log(this.form.getRawValue())
  //   }

  // public getErrorMsg(field: string): string {
  //   const Field:any = this.form.get(field);
  //   return Field.hasError('required') ? '此欄位必填' : Field.getError('invalid').message
  // }

  //   private emailVerifier(field: FormControl): ValidationErrors {
  //     return field.value.includes('gmail')
  //       ? {}
  //       : {
  //           invalid: {
  //             message: '信箱格式錯誤',
  //           },
  //         };
  //   }
  //   private passwordVerifier(field: FormControl): ValidationErrors {
  // return field.value.length>8 ? {} : {
  //   invalid: {
  //     message: '至少8個數字',
  //   }
  // }

  public log() {
    console.log(this.form);
  }
  public submit3() {
    if (this.form.valid) {
      this.toDetailPage();
    } else {
      this.isShowWarn = true;
    }
  }
  private toDetailPage() {
    // this.router.navigate(["giro-check-transacton-detail"]);
  }
}
