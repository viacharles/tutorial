import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdComponent } from './pages/ad/ad.component';
import { CarouselExampleComponent } from './pages/carousel-example/carousel-example.component';
import { CsvToJsonComponent } from './pages/csv-to-json/csv-to-json.component';
import { DateExampleComponent } from './pages/date-example/date-example.component';
import { OverlayControlCenterExampleComponent } from './pages/overlay-control-center-example/overlay-control-center-example.component';
import { ReactiveFormsExampleComponent } from './pages/reactive-forms-example/reactive-forms-example.component';
import { RxjsPracticeComponent } from './pages/rxjs-practice/rxjs-practice.component';
import { TestComponent } from './pages/test/test.component';
import { TouchmoveComponent } from './pages/touchmove/touchmove.component';
import { VirtualCurrencyComponent } from './pages/virtual-currency/virtual-currency.component';

const routes: Routes = [
  {path: "", pathMatch:'full', redirectTo:'overlay-control-center'},
  {path: "ad", component: AdComponent},
  {path: "carousel-example", component: CarouselExampleComponent},
  {path: "csv-to-json", component:CsvToJsonComponent,
loadChildren:() => import('./pages/csv-to-json/csv-to-json.module').then(m => m.CsvToJsonModule)
},
{path: "date-example", component: DateExampleComponent},
{path: "overlay-control-center-example", component: OverlayControlCenterExampleComponent
},
{path: "reactive-forms-example", component: ReactiveFormsExampleComponent},
{path: "rxjs-practice", component: RxjsPracticeComponent},
{path: "student-list",
loadChildren:()=>import('./pages/student-list/student-list.module').then(m=>m.StudentListModule)
},
{path: "test", component: TestComponent},
{path: "touchmove", component: TouchmoveComponent},
{path: "virtual-currency", component: VirtualCurrencyComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TutorialRoutingModule { }
