import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './modules/layout/layout.component';
import { OverlayModule } from './modules/tutorial/pages/overlay-control-center-example/overlays/overlay.module';
import { OverlayControlCenterComponent } from './modules/shared/components/overlay-control-center/overlay-control-center.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    OverlayControlCenterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OverlayModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
